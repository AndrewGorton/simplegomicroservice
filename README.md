# SimpleGoMicroservice

## Description

A simple Go microservice using [Go](http://golang.org), [GoRest](http://code.google.com/p/gorest/) and [Go-Uuid](https://godoc.org/code.google.com/p/go-uuid/uuid).

## Building and Running

```bash
$ go get code.google.com/p/gorest
$ go get code.google.com/p/go-uuid/uuid
$ go run src/*.go
2015/01/22 20:44:01 All EndPoints for service [ MessageService ] , registered under root path:  /
2015/01/22 20:44:01 Registerd service: MessageService  endpoint: GET
```

## Testing

```bash
$ curl localhost:8787/ | python -m json.tool
{
    "identifier": "86541463-0116-4778-9470-ddd846ee649b",
    "message": "Hello"
}
```

## Building a standalone binary

```bash
$ go build src/*.go
$ ./MainModule
2015/01/22 20:47:13 All EndPoints for service [ MessageService ] , registered under root path:  /
2015/01/22 20:47:13 Registerd service: MessageService  endpoint: GET
```




