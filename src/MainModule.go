package main

// You'll need to
//   go get code.google.com/p/gorest
//   go get code.google.com/p/go-uuid/uuid
// to get download the imports first

import (
	"code.google.com/p/gorest"
	"net/http"
)

// Program entry point.
//
// Registers the services with the GoRest framework, and starts the HTTP server
// on the specified port.
func main() {
	gorest.RegisterService(new(MessageService))
	http.Handle("/", gorest.Handle())
	http.ListenAndServe(":8787", nil)
}
