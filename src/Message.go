package main

import "code.google.com/p/go-uuid/uuid"

// Return type.
//
// Note that we're using annotations to correctly capitalise the fields when serializing, because
// Go requires the fields to be exportable (start with an uppercase letter) to serialize them.
type Message struct {
	Message    string `json:"message"`
	Identifier string `json:"identifier"`
}

// Constructor.
//
// Creates a new message with the specified text and a new UUID.
//
// Param: newMessage The message text.
//
// Returns: A Message object with the specified text and a uuid identifier.
func newMessage(newMessage string) Message {
	return Message{Identifier: uuid.New(), Message: newMessage}
}
