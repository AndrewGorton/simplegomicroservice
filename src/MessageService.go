package main

import (
	"code.google.com/p/gorest"
	"fmt"
	"time"
)

// Service configuration for the GoRest framework.
//
// We only add a single GET endpoint to demonstrate wiring it up.
type MessageService struct {
	// Service configuration
	gorest.RestService `root:"/" consumes:"application/json" produces:"application/json"`

	// URL handler added here. The name must match but be initial lowercase here, but uppercase for the function.
	getMessage gorest.EndPoint `method:"GET" path:"/" output:"Message"`
}

// Service handler.
//
// Name matches that in the service configuration struct above except it starts with an uppercase character.
//
// Returns a Message which is serialised to json.
func (serv MessageService) GetMessage() Message {
	fmt.Printf("In AMessage\n")
	return <-generateMessage()
}

// Worker function.
//
// Returns a channel upon which you can wait for the response to appear upon.
func generateMessage() <-chan Message {
	out := make(chan Message)

	// Gofunc which simulates work by sleeping for a second
	go func() {
		time.Sleep(1 * time.Second)
		out <- newMessage("Hello")
		close(out)
	}()

	return out
}
